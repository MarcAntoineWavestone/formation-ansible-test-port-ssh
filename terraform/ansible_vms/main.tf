# doc https://github.com/antonputra/tutorials/tree/main/lessons/014

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

locals {
  ssh_user         = "ubuntu"
  key_name         = "deployer_key"
  private_key_path = "/home/ubuntu/.ssh/id_rsa"
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer_key"
  public_key = file("/home/ubuntu/.ssh/id_rsa.pub")
}

################################################################################
# Compute Resources
################################################################################

resource "aws_instance" "nginx" {
  ami                         = "ami-08edbb0e85d6a0a07" # UBUNTU # "ami-04dd4500af104442f" # AWS-AMI # data.aws_ami.ubuntu.id
  subnet_id                   = aws_subnet.devops_front_subnet.id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  security_groups             = [aws_security_group.nginx_sg.id]
  key_name                    = local.key_name
  
  tags = {
    Name = "nginx_vm_${var.participant}"
    participant = "${var.participant}"
    type = "nginx"
  }
}

resource "aws_instance" "application" {
  ami                         = "ami-08edbb0e85d6a0a07" # UBUNTU # "ami-04dd4500af104442f" # AWS-AMI # data.aws_ami.ubuntu.id
  subnet_id                   = aws_subnet.devops_appli_subnet.id
  instance_type               = "t2.micro"
  associate_public_ip_address = false
  security_groups             = [aws_security_group.application_sg.id]
  key_name                    = local.key_name

  tags = {
    Name = "application_vm_${var.participant}"
    participant = "${var.participant}"
    type = "application"
  }
}

################################################################################
# Network Resources
################################################################################
data "aws_vpc" "devops_vpc" {
  tags = {
    Name = "devops_vpc"
  }
}

data "aws_route_table" "devops_rt" {
  tags = {
    Name = "devops_rt"
  }
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.devops_appli_subnet.id
  route_table_id = aws_route_table.devops_nat_rt.id
}

resource "aws_route_table_association" "c" {
  subnet_id      = aws_subnet.devops_front_subnet.id
  route_table_id = data.aws_route_table.devops_rt.id
}

resource "aws_subnet" "devops_appli_subnet" {
  vpc_id     = data.aws_vpc.devops_vpc.id
  cidr_block = "10.0.3.0/24"

  tags = {
    Name = "devops_appli_subnet"
  }
}

resource "aws_subnet" "devops_front_subnet" {
  vpc_id     = data.aws_vpc.devops_vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "devops_appli_subnet"
  }
}

resource "aws_security_group" "nginx_sg" {
  name   = "nginx_access"
  vpc_id = data.aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.devops_admin_subnet.cidr_block] 
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "application_sg" {
  name   = "application_access"
  vpc_id = data.aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.devops_admin_subnet.cidr_block] 
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.devops_front_subnet.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
