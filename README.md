# La formation Ansible

## Informations importantes
Ce projet Gitlab rassemble le code Terraform et Ansible qui te permettra de déployer ton environnement de travail sur AWS pour la formation. <br>
Afin de pouvoir déployer et utiliser ce projet sur ton propre projet Gitlab, il est nécessaire de suivre l'ensemble des étapes qui sont mentionnées dans la documentation suivante:
https://digiplace.sharepoint.com/:p:/s/WOD_ALL-NEXTGENIT/EQwYLWZzlIhIraBOtVogdGsBDdEeqiwu0K-VcjwA5aNuaw?e=99oOSw

## Conditions nécessaires :
- Suivre les étapes de la documentation
- Compte AWS
- Compte Gitlab

## Objectifs de la formation :​
Cette formation à l'ambition de vous donner les bases essentielles sur Ansible. Les objectifs de la formation sont les suivants:

- Comprendre les bénéfices de la gestion de configuration ​

- Savoir utiliser Ansible pour configurer des infrastructures​

- Connaître les bonnes pratiques d’utilisation d’Ansible

# Known issues
```
$ ansible -m ping all -i inventory 
10.0.1.184 | UNREACHABLE! => {
    "msg": "Failed to connect to the host via ssh: kex_exchange_identification: Connection closed by remote host\r\n",
    "unreachable": true
}
```
## Solution 
Avant d'essayer le ping, modifie la commande proxy dans le fichier ssh.cfg (changer bastion avec l'IP de celui-ci)  <br>
Ajoute la clé ssh en spécifiant le nom et le chemin de la clé `$ ssh-add ssh-key` <br>
Si tu utilises ubuntu sur AWS EC2, execute `$ ssh-agent bash` avant le ssh-add  <br>

## Commands
```
$ ansible -m ping all -i inventory 
$ ansible-playbook -i inventory nginx.yaml
```
